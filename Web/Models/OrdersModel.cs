﻿using ApplicationCore.Orders.Dto;
using ApplicationCore.Providers.Dto;
using System.Collections.Generic;

namespace Web.Models
{
    public class OrdersModel
    {
        public OrdersVm Orders { get; set; }
    }
}
