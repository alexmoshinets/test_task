﻿using ApplicationCore.OrderItems.Commands.CreateOrderItem;
using ApplicationCore.Orders.Commands.CreateOrder;
using ApplicationCore.Orders.Dto;
using ApplicationCore.Orders.Queries.GetOrder;
using ApplicationCore.Orders.Queries.GetOrders;
using ApplicationCore.Providers.Queries.GetProviders;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Orders.Commands.DeleteOrder;

namespace Web.Controllers
{
    public class HomeController : BaseController
    {
        public async Task<IActionResult> Index(int? providerId = null)
        {
            var query = new GetOrdersQuery{ ProviderId = providerId };
            var model = await Mediator.Send(query);
            return View(model);
        }

        public async Task<IActionResult> Detail(int id)
        {
            var model = await Mediator.Send(new GetOrderQuery(id));
            return View(model);
        }

        public async Task<IActionResult> Create()
        {
            var model = new OrderDto();
            await UpdateProviders(model);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(DeleteOrderCommand command)
        {
            await Mediator.Send(command);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> Create(OrderDto model)
        {
            if (!ModelState.IsValid)
            {
                await UpdateProviders(model);
                return View(model);
            }

            var commandOrder = new CreateOrderCommand
            {
                Number = model.Number,
                Date = DateTime.Now,
                ProviderId = model.ProviderId
            };
            var orderId = await Mediator.Send(commandOrder);

            foreach (var orderItem in model.OrderItems)
            {
                var commandOrderItem = new CreateOrderItemCommand
                {
                    Quantity = orderItem.Quantity,
                    Name = orderItem.Name,
                    Unit = orderItem.Unit,
                    OrderId = orderId
                };
                await Mediator.Send(commandOrderItem);
            }

            return RedirectToAction("Index");
        }

        private async Task UpdateProviders(OrderDto model)
        {
            model.Providers = (await Mediator.Send(new GetProvidersQuery())).Lists.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();
        }
    }
}
