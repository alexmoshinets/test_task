﻿using Domain.Entities.OrderAggregate;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Data
{
    public static class DataContextSeed
    {
        public static async Task SeedSampleDataAsync(DataContext context)
        {
            // Seed, if necessary
            if (!context.Providers.Any())
            {
                context.Providers.Add(new Provider("ООО Поставщик 1"));
                context.Providers.Add(new Provider("ЗАО Поставщик 2"));
                context.Providers.Add(new Provider("ПАО Поставщик 3"));

                await context.SaveChangesAsync();
            }
        }
    }
}
