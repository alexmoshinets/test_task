﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Entities.OrderAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Data.Configurations.Orders
{
    public class ProviderConfiguration : IEntityTypeConfiguration<Provider>
    {
        public void Configure(EntityTypeBuilder<Provider> builder)
        {
            var navigation = builder.Metadata.FindNavigation(nameof(Provider.Orders));
            navigation.SetPropertyAccessMode(PropertyAccessMode.Field);

            builder.Property(t => t.Name)
                .HasMaxLength(200)
                .IsRequired();
        }
    }
}
