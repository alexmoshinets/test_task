﻿using ApplicationCore.OrderItems.Interfaces;
using Domain.Entities.OrderAggregate;

namespace Infrastructure.Data.Repositories
{
    public class OrderItemRepository : EfRepository<OrderItem>, IOrderItemRepository
    {
        public OrderItemRepository(DataContext dbContext) : base(dbContext)
        {
        }
    }
}
