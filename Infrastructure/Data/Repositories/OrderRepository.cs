﻿using ApplicationCore.Orders.Interfaces;
using Domain.Entities.OrderAggregate;


namespace Infrastructure.Data.Repositories
{
    public class OrderRepository : EfRepository<Order>, IOrderRepository
    {
        public OrderRepository(DataContext dbContext) : base(dbContext)
        {
        }
    }
}
