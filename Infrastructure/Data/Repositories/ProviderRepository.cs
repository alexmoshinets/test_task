﻿using ApplicationCore.Providers.Interfaces;
using Domain.Entities.OrderAggregate;

namespace Infrastructure.Data.Repositories
{
    public class ProviderRepository : EfRepository<Provider>, IProviderRepository
    {
        public ProviderRepository(DataContext dbContext) : base(dbContext)
        {
        }
    }
}
