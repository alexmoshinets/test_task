﻿using ApplicationCore.Orders.Dto;
using MediatR;

namespace ApplicationCore.Orders.Queries.GetOrders
{
    public class GetOrdersQuery: IRequest<OrdersVm>
    {
        public int? ProviderId { get; set; }
    }
}
