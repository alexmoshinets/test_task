﻿using ApplicationCore.Orders.Dto;
using ApplicationCore.Orders.Interfaces;
using ApplicationCore.Orders.Specifications;
using ApplicationCore.Providers.Interfaces;
using AutoMapper;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ApplicationCore.Providers.Queries.GetProviders;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ApplicationCore.Orders.Queries.GetOrders
{
    public class GetOrdersQueryHandler : IRequestHandler<GetOrdersQuery, OrdersVm>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IProviderRepository _providerRepository;
        private readonly IMapper _mapper;

        public GetOrdersQueryHandler(IOrderRepository orderRepository, IProviderRepository providerRepository, IMapper mapper)
        {
            _orderRepository = orderRepository;
            _providerRepository = providerRepository;
            _mapper = mapper;
        }

        public async Task<OrdersVm> Handle(GetOrdersQuery request, CancellationToken cancellationToken)
        {
            var specification = new OrderWithOrderItemsSpecification(request.ProviderId);
            var data = await _orderRepository.ListAsync(specification);
            var model = new OrdersVm
            {
                Lists = data.Select(_mapper.Map<OrderDto>)
                    .OrderByDescending(t => t.Date).ToList(),
                Providers = (await _providerRepository.ListAllAsync()).Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                }).ToList()
            };
            return model;
        }
    }
}
