﻿using System.Linq;
using ApplicationCore.Orders.Dto;
using ApplicationCore.Orders.Interfaces;
using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ApplicationCore.Orders.Specifications;
using ApplicationCore.Providers.Interfaces;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ApplicationCore.Orders.Queries.GetOrder
{
    public class GetOrderQueryHandler : IRequestHandler<GetOrderQuery, OrderDto>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IProviderRepository _providerRepository;
        private readonly IMapper _mapper;

        public GetOrderQueryHandler(IOrderRepository orderRepository, IProviderRepository providerRepository, IMapper mapper)
        {
            _orderRepository = orderRepository;
            _providerRepository = providerRepository;
            _mapper = mapper;
        }

        public async Task<OrderDto> Handle(GetOrderQuery request, CancellationToken cancellationToken)
        {
            var specification = new OrderGetByIdSpecification(request.Id);
            var data = (await _orderRepository.ListAsync(specification)).FirstOrDefault();
            var model = _mapper.Map<OrderDto>(data);
            var providers = await _providerRepository.ListAllAsync();
            model.Providers = providers.Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString(),
            }).ToList();
            return model;
        }
    }
}
