﻿using ApplicationCore.Orders.Dto;
using MediatR;

namespace ApplicationCore.Orders.Queries.GetOrder
{
    public class GetOrderQuery: IRequest<OrderDto>
    {
        public int Id { get; set; }

        public GetOrderQuery(int id)
        {
            Id = id;
        }
    }
}
