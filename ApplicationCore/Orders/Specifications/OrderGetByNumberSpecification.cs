﻿using System;
using System.Collections.Generic;
using System.Text;
using ApplicationCore.Common.Specifications;
using Domain.Entities.OrderAggregate;

namespace ApplicationCore.Orders.Specifications
{
    public sealed class OrderGetByNumberSpecification : BaseSpecification<Order>
    {
        public OrderGetByNumberSpecification(string number)
            : base(t => t.Number == number)
        {
            
        }
    }
}
