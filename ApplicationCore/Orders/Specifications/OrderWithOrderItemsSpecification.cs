﻿using ApplicationCore.Common.Specifications;
using Domain.Entities.OrderAggregate;

namespace ApplicationCore.Orders.Specifications
{
    public sealed class OrderWithOrderItemsSpecification : BaseSpecification<Order>
    {
        public OrderWithOrderItemsSpecification(int? providerId)
            : base(t => providerId.HasValue && providerId.Value == t.ProviderId || !providerId.HasValue)
        {
            AddIncludes(query => query.Include(o => o.Provider));
        }
    }
}
