﻿using ApplicationCore.Common.Specifications;
using Domain.Entities.OrderAggregate;

namespace ApplicationCore.Orders.Specifications
{
    public sealed class OrderGetByIdSpecification : BaseSpecification<Order>
    {
        public OrderGetByIdSpecification(int id)
            : base(t => t.Id == id)
        {
            AddIncludes(query => query.Include(o => o.Provider));
            AddIncludes(query => query.Include(o => o.OrderItems));
        }
    }
}
