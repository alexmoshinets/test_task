﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ApplicationCore.Orders.Dto
{
    public class OrdersVm
    {
        public OrdersVm()
        {
            Lists = new List<OrderDto>();
        }

        public IList<SelectListItem> Providers { get; set; }
        public int ProviderId { get; set; }

        public IList<OrderDto> Lists { get; set; }
    }
}
