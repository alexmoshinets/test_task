﻿using ApplicationCore.Common.Mappings;
using AutoMapper;
using Domain.Entities.OrderAggregate;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ApplicationCore.Providers.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ApplicationCore.Orders.Dto
{
    public class OrderDto : IMapFrom<Order>
    {
        public OrderDto()
        {
            OrderItems = new List<OrderItemDto>();
        }

        public int Id { get; set; }

        [Required]
        public string Number { get; set; }
        public DateTime Date { get; set; }

        [Required]
        public int ProviderId { get; set; }
        public ProviderDto Provider { get; set; }
        public List<SelectListItem> Providers { get; set; }

        public IList<OrderItemDto> OrderItems { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Order, OrderDto>();
        }
    }
}
