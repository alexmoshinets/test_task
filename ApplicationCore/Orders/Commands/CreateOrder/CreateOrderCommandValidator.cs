﻿using ApplicationCore.Orders.Interfaces;
using ApplicationCore.Orders.Specifications;
using FluentValidation;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ApplicationCore.Orders.Commands.CreateOrder
{
    public class CreateOrderCommandValidator : AbstractValidator<CreateOrderCommand>
    {
        private readonly IOrderRepository _orderRepository;

        public CreateOrderCommandValidator(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;

            RuleFor(v => v.Number)
                .NotEmpty().WithMessage("Number is required.")
                .MaximumLength(200).WithMessage("Number must not exceed 200 characters.")
                .MustAsync(BeUniqueNumber).WithMessage("The specified number already exists.");
        }

        public async Task<bool> BeUniqueNumber(string number, CancellationToken cancellationToken)
        {
            var data = await _orderRepository.ListAsync(new OrderGetByNumberSpecification(number));
            return !data.Any();
        }
    }
}
