﻿using ApplicationCore.Orders.Interfaces;
using Domain.Entities.OrderAggregate;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ApplicationCore.Orders.Commands.CreateOrder
{
    public class CreateOrderCommandHandler : IRequestHandler<CreateOrderCommand, int>
    {
        private readonly IOrderRepository _orderRepository;

        public CreateOrderCommandHandler(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public async Task<int> Handle(CreateOrderCommand request, CancellationToken cancellationToken)
        {
            var entity = new Order(request.Number, request.ProviderId, DateTime.Now);
            await _orderRepository.AddAsync(entity);
            return entity.Id;
        }
    }
}
