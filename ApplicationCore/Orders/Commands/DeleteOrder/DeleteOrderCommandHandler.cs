﻿using ApplicationCore.Orders.Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ApplicationCore.Orders.Commands.DeleteOrder
{
    public class DeleteOrderCommandHandler : IRequestHandler<DeleteOrderCommand, bool>
    {
        private readonly IOrderRepository _orderRepository;

        public DeleteOrderCommandHandler(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public async Task<bool> Handle(DeleteOrderCommand request, CancellationToken cancellationToken)
        {
            var entity = await _orderRepository.GetByIdAsync(request.Id);
            if (entity == null)
                return false;
            await _orderRepository.DeleteAsync(entity);
            return true;
        }
    }
}
