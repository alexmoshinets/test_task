﻿using System;
using System.Collections.Generic;
using System.Text;
using ApplicationCore.Common.Interfaces;
using Domain.Entities.OrderAggregate;

namespace ApplicationCore.Orders.Interfaces
{
    public interface IOrderRepository : IAsyncRepository<Order>
    {
    }
}
