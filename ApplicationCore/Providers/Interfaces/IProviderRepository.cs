﻿using ApplicationCore.Common.Interfaces;
using Domain.Entities.OrderAggregate;

namespace ApplicationCore.Providers.Interfaces
{
    public interface IProviderRepository : IAsyncRepository<Provider>
    {
    }
}
