﻿using ApplicationCore.Common.Mappings;
using AutoMapper;
using Domain.Entities.OrderAggregate;

namespace ApplicationCore.Providers.Dto
{
    public class ProviderDto : IMapFrom<Provider>
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Provider, ProviderDto>();
        }
    }
}
