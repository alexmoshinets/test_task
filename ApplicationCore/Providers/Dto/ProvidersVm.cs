﻿using System.Collections.Generic;

namespace ApplicationCore.Providers.Dto
{
    public class ProvidersVm
    {
        public ProvidersVm()
        {
            Lists = new List<ProviderDto>();
        }

        public IList<ProviderDto> Lists { get; set; }
    }
}
