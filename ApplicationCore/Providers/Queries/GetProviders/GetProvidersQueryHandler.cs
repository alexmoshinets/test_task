﻿using ApplicationCore.Providers.Dto;
using ApplicationCore.Providers.Interfaces;
using AutoMapper;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ApplicationCore.Providers.Queries.GetProviders
{
    public class GetProvidersQueryHandler : IRequestHandler<GetProvidersQuery, ProvidersVm>
    {
        private readonly IProviderRepository _providerRepository;
        private readonly IMapper _mapper;

        public GetProvidersQueryHandler(IProviderRepository providerRepository, IMapper mapper)
        {
            _providerRepository = providerRepository;
            _mapper = mapper;
        }

        public async Task<ProvidersVm> Handle(GetProvidersQuery request, CancellationToken cancellationToken)
        {
            var data = await _providerRepository.ListAllAsync();
            return new ProvidersVm
            {
                Lists = data.Select(_mapper.Map<ProviderDto>)
                    .OrderBy(t => t.Id).ToList()
            };
        }
    }
}
