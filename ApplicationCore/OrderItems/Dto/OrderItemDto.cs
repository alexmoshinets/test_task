﻿using System;
using System.Collections.Generic;
using System.Text;
using ApplicationCore.Common.Mappings;
using AutoMapper;
using Domain.Entities.OrderAggregate;

namespace ApplicationCore.OrderItems.Dto
{
    public class OrderItemDto : IMapFrom<OrderItem>
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public string Name { get; set; }
        public decimal Quantity { get; set; }
        public string Unit { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<OrderItem, OrderItemDto>();
        }
    }
}
