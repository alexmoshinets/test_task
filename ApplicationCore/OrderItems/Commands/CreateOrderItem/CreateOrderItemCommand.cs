﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace ApplicationCore.OrderItems.Commands.CreateOrderItem
{
    public class CreateOrderItemCommand : IRequest<int>
    {
        public int OrderId { get; set; }
        public string Name { get; set; }
        public decimal Quantity { get; set; }
        public string Unit { get; set; }
    }
}
