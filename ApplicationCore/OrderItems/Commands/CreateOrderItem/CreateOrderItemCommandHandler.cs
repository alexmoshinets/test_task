﻿using ApplicationCore.OrderItems.Interfaces;
using Domain.Entities.OrderAggregate;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ApplicationCore.OrderItems.Commands.CreateOrderItem
{
    public class CreateOrderItemCommandHandler : IRequestHandler<CreateOrderItemCommand, int>
    {
        private readonly IOrderItemRepository _orderItemRepository;

        public CreateOrderItemCommandHandler(IOrderItemRepository orderItemRepository)
        {
            _orderItemRepository = orderItemRepository;
        }

        public async Task<int> Handle(CreateOrderItemCommand request, CancellationToken cancellationToken)
        {
            var entity = new OrderItem(request.Name, request.OrderId, request.Quantity, request.Unit);
            await _orderItemRepository.AddAsync(entity);
            return entity.Id;
        }
    }
}
