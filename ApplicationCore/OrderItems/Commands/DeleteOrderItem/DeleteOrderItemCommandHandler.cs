﻿using ApplicationCore.OrderItems.Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ApplicationCore.OrderItems.Commands.DeleteOrderItem
{
    public class DeleteOrderItemCommandHandler : IRequestHandler<DeleteOrderItemCommand, bool>
    {
        private readonly IOrderItemRepository _orderItemRepository;

        public DeleteOrderItemCommandHandler(IOrderItemRepository orderItemRepository)
        {
            _orderItemRepository = orderItemRepository;
        }

        public async Task<bool> Handle(DeleteOrderItemCommand request, CancellationToken cancellationToken)
        {
            var entity = await _orderItemRepository.GetByIdAsync(request.Id);
            if (entity == null)
                return false;
            await _orderItemRepository.DeleteAsync(entity);
            return true;
        }
    }
}
