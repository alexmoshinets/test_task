﻿using MediatR;

namespace ApplicationCore.OrderItems.Commands.DeleteOrderItem
{
    public class DeleteOrderItemCommand : IRequest<bool>
    {
        public int Id { get; set; }
    }
}
