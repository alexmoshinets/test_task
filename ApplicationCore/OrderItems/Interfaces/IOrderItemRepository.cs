﻿using ApplicationCore.Common.Interfaces;
using Domain.Entities.OrderAggregate;

namespace ApplicationCore.OrderItems.Interfaces
{
    public interface IOrderItemRepository : IAsyncRepository<OrderItem>
    {
    }
}
