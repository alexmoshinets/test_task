﻿using System.ComponentModel.DataAnnotations;
using Domain.Common;

namespace Domain.Entities.OrderAggregate
{
    public class OrderItem : BaseEntity, IAggregateRoot
    {
        public OrderItem(string name, int orderId, decimal quantity, string unit)
        {
            Name = name;
            OrderId = orderId;
            Quantity = quantity;
            Unit = unit;
        }

        public int OrderId { get; private set; }
        public Order Order { get; private set; }

        [Required]
        public string Name { get; private set; }

        [Required]
        public decimal Quantity { get; private set; }

        [Required]
        public string Unit { get; private set; }

        public void Update(string name, int orderId, decimal quantity, string unit)
        {
            Name = name;
            OrderId = orderId;
            Quantity = quantity;
            Unit = unit;
        }
    }
}
