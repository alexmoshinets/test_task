﻿using Domain.Common;
using System.Collections.Generic;

namespace Domain.Entities.OrderAggregate
{
    public class Provider : BaseEntity, IAggregateRoot
    {
        public Provider(string name)
        {
            Name = name;
        }

        public string Name { get; private set; }


        private readonly List<Order> _orders = new List<Order>();
        public IReadOnlyCollection<Order> Orders => _orders.AsReadOnly();
    }
}
