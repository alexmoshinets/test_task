﻿using System;
using System.Collections.Generic;
using System.Text;
using Ardalis.GuardClauses;
using Domain.Common;

namespace Domain.Entities.OrderAggregate
{
    public class Order : BaseEntity, IAggregateRoot
    {
        public Order(string number, int providerId, DateTime date)
        {
            Number = number;
            ProviderId = providerId;
            Date = date;
        }

        public string Number { get; private set; }
        public DateTime Date { get; private set; }

        private readonly List<OrderItem> _orderItems = new List<OrderItem>();
        public IReadOnlyCollection<OrderItem> OrderItems => _orderItems.AsReadOnly();

        public int ProviderId { get; private set; }
        public Provider Provider { get; private set; }

        public void Update(string number, int providerId, DateTime date)
        {
            Number = number;
            ProviderId = providerId;
            Date = date;
        }
    }
}
